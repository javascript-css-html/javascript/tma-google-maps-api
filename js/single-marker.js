// Invoke strict mode.
"use strict";

// Create a new 'Sanitizer' instance for sanitising of HTML.
const sanitizer = new Sanitizer();

// GOOGLE MAPS API.

// 1. 'initMap()': a callback function for use with Google Maps API.
function initMap() {
    // 1a. 'options': variable to store map settings.
    const options = {
        zoom: 14.5, 
        center: {
            lat: -33.81692, 
            lng: 151.08333
        }
    }
    // 1b. 'map': creates a new instance of a Google Map, identifies the HTML id and adds map options.
    const map = new google.maps.Map(document.getElementById('map'), options);
    // 1c. 'marker': Adds an instance of positioned Google Marker.
    const marker = new google.maps.Marker({
        position: {
            lat: -33.81632, 
            lng: 151.08842
        }, 
        map: map, 
        icon: './images/favicon-32x32.png'
    });
    // 1d. 'infoContent': contains content for an instance of a Google InfoWindow.
    const infoContent = `<h1>RedAndBlackZone</h1>
                            <h4>Contrast is good</h4>
                            <h2>Web Design</h2>
                        `
    const infoWindow = new google.maps.InfoWindow({
        content: infoContent
    });
    marker.addListener("click", () => {
        infoWindow.open(map, marker);
    });
}