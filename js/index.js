// Invoke strict mode.
"use strict";

// Create a new 'Sanitizer' instance for sanitising of HTML.
const sanitizer = new Sanitizer();

// GOOGLE MAPS API.

// 1. 'initMap()': a callback function for use with Google Maps API.
function initMap() {
    // 1a. 'options': variable to store map settings.
    const options = {
        zoom: 14.5, 
        center: {
            lat: -33.81692, 
            lng: 151.08333
        }
    }
    // 1b. 'map': creates a new instance of a Google Map, identifies the HTML id and adds map options.
    const map = new google.maps.Map(document.getElementById('map'), options);
    // 1c. 'map': adds an event listener when map is clicked a createMarker() function is run.
    google.maps.event.addListener(map, 'click', function(event) {
        addMarker(
            {
                coordinates: event.latLng, 
                content: `Location: ${event.latLng}`
            }
        );
    });
    // 1d. 'markers': an array to store multiple markers to be called by the 'addMarker' function.
    let markers = [
        {
            coordinates: {
                lat: -33.81632, 
                lng: 151.08842
            },
            markerIcon: './images/favicon-32x32.png',
            content:    `<h1>RedAndBlackZone</h1>
            <h4>Contrast is good</h4>
            <h2>Web Design</h2>
            `
        }, 
        {
            coordinates: {
                lat: -33.81950, 
                lng: 151.08742
            },
            content: `<h1>Meadowbank Memorial Park</h1>`
        }, 
        {
            coordinates: {
                lat: -33.81398, 
                lng: 151.09166
            },
            content: `<h1>Meadowbank TAFE</h1>`
        }
    ];
    // 1e. 'markers': for loop will display all markers within the 'markers' array..
    for(let marker of markers) {
        addMarker(marker);
    }
    function addMarker(props) {
        // 1e1. 'marker': Adds an instance of positioned Google Marker.
        const marker = new google.maps.Marker({
            position: props.coordinates, 
            map: map,
            icon: props.markerIcon,
            content: props.content
        });
        // 1e2. 'setIcon': conditional statement to assign a value to 'markerIcon' if none is specified.
        // Otherwise the 'markerIcon' value will be undefined.
        if(props.markerIcon) {
            marker.setIcon(props.markerIcon);
        }
        // 1e3. 'infoWindow': an instance of a Google InfoWindow and listens out for click to reveal info content.
        if(props.content) {
            const infoWindow = new google.maps.InfoWindow({
                content: props.content
            });
            marker.addListener("click", () => {
                infoWindow.open(map, marker);
            });
        }
    }
}