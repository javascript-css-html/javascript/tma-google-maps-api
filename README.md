# tma-google-maps-api

## Description
followed along with YouTube video "Google Maps JavaScript API Tutorial" by Brad Traversy of Traversy Media. Link: https://youtu.be/Zxf1mnP5zcw.

Now I would like to have the event listener to display near by locations instead of just the latitude and longitude.

## Badges
![20% HTML](https://img.shields.io/static/v1?label=HTML&message=20%&color=blue)
![80% JavaScript](https://img.shields.io/static/v1?label=JavaScript&message=80%&color=yellow)
![80% Complete](https://img.shields.io/static/v1?label=Completed&message=80%&color=green)

## Visual Status of Project
![Current project status.](./images/2022-09-22-at-05-32-58-pm-tma-google-maps-api.png)

## Installation
For guidance go to this Google Maps documentation: https://developers.google.com/maps/documentation/javascript/adding-a-google-map. 

Additional Documentation for:

Customising Markers: https://developers.google.com/maps/documentation/javascript/custom-markers.
Info Windows: https://developers.google.com/maps/documentation/javascript/infowindows#maps_infowindow_simple-typescript.

## Usage
Standard Google Map to show location of a suburb (in this case Meadowbank, Sydney, Australia where I live). See: https://www.google.com.au/maps/place/Meadowbank+NSW+2114/@-33.8162025,151.0838562,16z/data=!3m1!4b1!4m5!3m4!1s0x6b12a455e462e28d:0x5017d681632c090!8m2!3d-33.8157836!4d151.0896983.

## Support
Create an issue or Just send me a message.

## Contributing
You are welcome to fork and improve as you wish.

## Authors and acknowledgment
YouTube video "Google Maps JavaScript API Tutorial" by Brad Traversy of Traversy Media.

Link: https://youtu.be/Zxf1mnP5zcw.

## License
No license.
